import random
import copy
import time
import math

point_4 = [[0,0],[0,2],[2,0],[2,2]]
point_6 = [[0,1],[1,0],[1,2],[2,1]]
point_3 = [[1,1]]

INFINITY = 10000000
WIN = 10000
TIME_LIMIT = 15

class Team39():
    def __init__(self):
        self.cntx = 0
        self.cnto = 0
        self.cntd = 0
        self.lastmove = (-1,-1,-1)

    def move(self, board, old_move, flag):
        self.player = flag
        self.timedout = 0
        self.start = time.time()

        if self.lastmove != (-1,-1,-1):
            a = int(math.floor(self.lastmove[1]/3))
            b = int(math.floor(self.lastmove[2]/3))    
            point = 100
            upd1 = board.small_boards_status[self.lastmove[0]][a][b]
            if [a,b] in point_3:
                point = 300
            elif [a,b] in point_4:
                point = 400
            elif [a,b] in point_6:
                point = 600
            if  upd1 == 'x':
                self.cntx += point
            elif upd1 == 'o':
                self.cnto += point
            elif upd1 == 'd':
                self.cntd += point

        a = int(math.floor(old_move[1]/3))
        b = int(math.floor(old_move[2]/3))
           
        point = 1
        upd2 = board.small_boards_status[old_move[0]][a][b]
        if [a,b] in point_3:
            point = 3
        elif [a,b] in point_4:
            point = 4
        elif [a,b] in point_6:
            point = 6
        if  upd2 == 'x':
            self.cntx += point
        elif upd2 == 'o':
            self.cnto += point
        elif upd2 == 'd':
            self.cntd += point


        cells = board.find_valid_move_cells(old_move)
        max_score = -1 * INFINITY
        best_move = cells[random.randrange(len(cells))]
        depth = 2
        self.starting_depth = 2
        while self.timedout == 0:
            for valid_move in cells:
                score = self.alphabeta(board, old_move, valid_move, -1*INFINITY, INFINITY, 0, depth,self.cntx,self.cnto,self.cntd)
                #1 is maximizer and 0 is minimizer
                if score > max_score:
                    max_score = score
                    best_move = valid_move
            self.lastmove = best_move
            depth += 1
            self.starting_depth = depth

        board.big_boards_status[best_move[0]][best_move[1]][best_move[2]] = "-"
        board.small_boards_status[best_move[0]][best_move[1]/3][best_move[2]/3] = "-"

        return best_move

    def utility(self, board, old_move, valid_move, alpha, beta, max_or_min, depth,cntx,cnto,cntd):
        winner, status = board.find_terminal_state()
        if status == "DRAW" or status == '-':               
            if self.player == 'x':
                return (cntx - cnto)
            elif self.player == 'o': 
                return (cnto - cntx)
        ### Need to adjust from the point of view of scoring ###
        elif status == "WON":
            if winner == self.player:
                return WIN - self.starting_depth*100
            else:
                return -1 * WIN - - self.starting_depth*100

    def alphabeta(self, board, old_move, valid_move, alpha, beta, max_or_min, depth,cntx,cnto,cntd):

        cells = board.find_valid_move_cells(valid_move)
        extra_score = 0

        #To prefer move where player goes to disabled smallboard
        if len(cells) > 18:
            if max_or_min == 0:
                extra_score = -200
            elif max_or_min == 1:
                extra_score = 200

        if self.timedout == 1 or time.time() - self.start > TIME_LIMIT:
            self.timedout = 1
            return extra_score+self.utility(board, old_move, valid_move, alpha, beta, max_or_min, depth,cntx,cnto,cntd)

        if len(cells) == 0 or depth == 0: #end state
            return extra_score+self.utility(board, old_move, valid_move, alpha, beta, max_or_min, depth,cntx,cnto,cntd)
            
        #Board is updated and passed to children
        #board = copy.deepcopy(board)
        board.update(old_move, valid_move, self.player)
        a = int(math.floor(valid_move[1]/3))
        b = int(math.floor(valid_move[2]/3))
        
        point = 100
        upd = board.small_boards_status[valid_move[0]][a][b]
        if [a,b] in point_3:
            point = 300
        elif [a,b] in point_4:
            point = 400
        elif [a,b] in point_6:
            point = 600
        sx = so = sd = 0
        if  upd == 'x':
            sx = point
        elif upd == 'o':
            so = point
        elif upd == 'd':
            sd = point

        #Bonus Adjustment
        bonus = 0
        if self.player == upd:
            bonus = 1
            extra_score += 200

        if max_or_min == 0:
        #Minimizer will choose the minimum out of children            
            minimum = INFINITY
            for cell in cells:
                res = self.alphabeta(board, valid_move, cell, alpha, beta, 1, depth-1,cntx+sx,cnto+so,cntd+sd)
                minimum = min(res, minimum)
                beta = min(res,beta)
                if beta <= alpha:
                    break
            board.big_boards_status[valid_move[0]][valid_move[1]][valid_move[2]] = "-"
            board.small_boards_status[valid_move[0]][valid_move[1]/3][valid_move[2]/3] = "-"

            #self.reverseupdate(board, old_move, valid_move, self.player)
            return extra_score+minimum

        else:
        #Maximizer will choose the maximum out of children
            maximum = -1 * INFINITY
            for cell in cells:
                res = self.alphabeta(board, valid_move, cell, alpha, beta, bonus | 0, depth-1,cntx+sx,cnto+so,cntd+sd)
                maximum = max(res, maximum)
                alpha = max(res,alpha)
                if beta <= alpha:
                    break
            board.big_boards_status[valid_move[0]][valid_move[1]][valid_move[2]] = "-"
            board.small_boards_status[valid_move[0]][valid_move[1]/3][valid_move[2]/3] = "-"

            #self.reverseupdate(board, old_move, valid_move, self.player)
            return extra_score+maximum

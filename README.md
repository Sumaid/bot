Xtreme Tic-Tac-Toe - AI Assignment Intermediate Report

                                       Team No: 39
                    WeWhoShallNotBeNamed
                           R S Subbulakshmi : 20171069
                    Syed Sumaid Ali   : 20171092
											
Search Algorithm:	
Minimax Algorithm(Alpha-Beta Pruning)		

Pros:
1)Minimax can give an optimal solution depending on heuristic function  being used
2)Minimax algorithms are guranteed to give correct solution, while other search algorithms sometimes rely on chance
3)It will take lot of time for Monte Carlo Search Tree to converge with minimax search tree, but minimax does not require the past long history to proceed
4)Minimax search tree can be very efficient by utilizing the domain specific knowledge

Cons:
1)When the branching factor is huge, minimax can be very slow.
2)A good minimax algorithm would require development of a good heuristic function
3)Monte Carlo search algorithm does not need domain specific knowledge, and hence can be easily incorporated into any other application also

Current Heuristic:

1)Game won returns maximum reward, game loss returns minimum reward
2)Game draw returns number of small boards won minus number of small boards lost
  Each of the small boards have weightages according to scoring method

Other Optimizations:	

1)To make sure goal notes with less depth are preferred, depth is subtracted from return value of minimax function for maximizer case, and added for minimizer case - ?
2)Iterative Deepening Depth First - ?

                                   USAGE

1)simulator.py - NOT TO BE TOUCHED (Tournament File)
2)simulated.py - Live Game Progress
3)tester.py - Number of games can be specified to get wins/losses statistics, also gives average time
 1 => Random player vs. Random player
 2 => Human vs. Random Player
 3 => Human vs. Human
 4 => Team39 vs. Random Player
 5 => Team39 vs. Human Player                                                                 
 6 => Team39 vs. Team39                                                                 

                                   REPORT

Random Agent vs Random Agent:
Wins : 55% 
Average Time of a game: 0.016

Random Agent vs Alpha-Beta Pruning agent(With simple heuristic):
Wins : 95%
Average Time of a game : 5
